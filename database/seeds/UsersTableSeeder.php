<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@local.host',
            'password' => bcrypt('admin'),
            'phone' => '081353444342',
            'address' => 'Jl. Local Host G5/A17, Laptop',
            'role' => 'admin',
            'photo' => NULL,
        ]);
    }
}
