<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@local.host',
            'password' => bcrypt('user'),
            'phone' => '081353444342',
            'address' => 'Jl. Local Host G5/A17, Laptop',
            'role' => 'user',
            'photo' => NULL,
        ]);
    }
}
