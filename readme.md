## About RTV <a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>


Aplikasi yang digunakan untuk merating berbagai macam Film ataupun Acara TV seperti imdb

## User
#### Halaman User
<img src="https://cdn-images-1.medium.com/max/800/1*0A7j6yFjMkrTLN9np-dRvQ.png">

#### Dashboard User &amp; List Film
<img src="https://cdn-images-1.medium.com/max/800/1*lA5A3MxcTiZ0wEdozvNrVQ.png">

#### Fitur Rating
<img src="https://cdn-images-1.medium.com/max/800/1*rvBBompzpoVE56Fj7dT3Hg.png">

#### Film Rank berdasar Rating
<img src="https://cdn-images-1.medium.com/max/800/1*knebKgvdlxoeTvIT8a1iiQ.png">

## Admin
#### Dashboard Admin
<img src="https://cdn-images-1.medium.com/max/800/1*BnXboe1E6_JF9Ql1r26Akw.png">

#### List User
<img src="https://cdn-images-1.medium.com/max/800/1*HR-K-26g4ryNdCL_wt-QSg.png">

#### Create Film
<img src="https://cdn-images-1.medium.com/max/800/1*6KcW9uwNsVVxuzl_TZ67OQ.png">

## Stack

- Laravel 5.8
- Mysql
- Paspport

## Setup

- clone
```
git clone
```

- install
```
composer install
```

- setup env
- migrate
```
php artisan migrate
```
```
php artisan db:seed
```
```
php artisan passport:install
```