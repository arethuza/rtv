@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<!-- Card stats -->
			<div class="row">
				<div class="col-xl-4 col-lg-4">
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<h5 class="card-title text-uppercase text-muted mb-0">Total Film</h5>
									<span class="h2 font-weight-bold mb-0">{{ count($dataFilm) }} Film</span>
								</div>
								<div class="col-auto">
									<div class="icon icon-shape bg-danger text-white rounded-circle shadow">
										<i class="fas fa-chart-bar"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4">
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<h5 class="card-title text-uppercase text-muted mb-0">Jumlah Pengguna</h5>
									<span class="h2 font-weight-bold mb-0">{{ count($dataUser) }} User</span>
								</div>
								<div class="col-auto">
									<div class="icon icon-shape bg-warning text-white rounded-circle shadow">
										<i class="fas fa-chart-pie"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4">
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<h5 class="card-title text-uppercase text-muted mb-0">Film yang sudah Dirating</h5>
									<span class="h2 font-weight-bold mb-0">{{ count($sudahRating) }} Film</span>
								</div>
								<div class="col-auto">
									<div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
										<i class="fas fa-users"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<!-- Table -->
	<div class="row">
		<div class="col">
			<div class="card shadow">
				<div class="card-header border-0">
					<h3 class="mb-0">Card tables</h3>
				</div>
				<div class="table-responsive">
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col">Rank</th>
								<th scope="col">Film / TV Shows</th>
								<th scope="col">Status</th>
								<th scope="col">Rated By</th>
								<th scope="col">Total Stars</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@foreach($dataFilm as $flm)
							<tr>
								<th scope="row">
									{{ $number++ }}
								</th>
								<th scope="row">
									<div class="media align-items-center">
										<a href="#" class="avatar rounded-circle mr-3">
											<img alt="Image placeholder" src="/photo/film/{{ $flm->photo }}">
										</a>
										<div class="media-body">
											<span class="mb-0 text-sm">{{ $flm->films_name }}</span>
										</div>
									</div>
								</th>
								<td>
									<span class="badge badge-dot">
										<i class="bg-success"></i> completed
									</span>
								</td>
								<td>
									{{ $flm->five_star + $flm->four_star + $flm->three_star + $flm->two_star + $flm->one_star }} Users
								</td>
								<td>
									{{ $flm->total_stars }} Stars
								</td>
								<td>
									<a href="/rank/detail/{{ $flm->id }}" class="btn btn-info btn-sm">Detail</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('rtv.base.footer')