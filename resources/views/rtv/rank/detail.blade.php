@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-8 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-12">
							<h3 class="mb-0">Beri rating untuk Film, {{ $dataFilm->films_name }}</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<style type="text/css">
						.rate {
							float: left;
							height: 46px;
							padding: 0 10px;
						}
						.rate:not(:checked) > input {
							position:absolute;
							top:-9999px;
						}
						.rate:not(:checked) > label {
							float:right;
							width:1em;
							overflow:hidden;
							white-space:nowrap;
							cursor:pointer;
							font-size:50px;
							color:#ccc;
							margin: -50px 0 0 0;
						}
						.rate:not(:checked) > label:before {
							content: '★ ';
						}
						.rate > input:checked ~ label {
							color: #ffc700;    
						}
						.rate:not(:checked) > label:hover,
						.rate:not(:checked) > label:hover ~ label {
							color: #deb217;  
						}
						.rate > input:checked + label:hover,
						.rate > input:checked + label:hover ~ label,
						.rate > input:checked ~ label:hover,
						.rate > input:checked ~ label:hover ~ label,
						.rate > label:hover ~ input:checked ~ label {
							color: #c59b08;
						}

						/* The bar container */
						.bar-container {
							width: 100%;
							background-color: #f1f1f1;
							text-align: center;
							color: white;
						}

						/* Individual bars */
						.bar-5 {width: {{ $p5 }}%; height: 18px; background-color: #4CAF50;}
						.bar-4 {width: {{ $p4 }}%; height: 18px; background-color: #2196F3;}
						.bar-3 {width: {{ $p3 }}%; height: 18px; background-color: #00bcd4;}
						.bar-2 {width: {{ $p2 }}%; height: 18px; background-color: #ff9800;}
						.bar-1 {width: {{ $p1 }}%; height: 18px; background-color: #f44336;}

						/* Three column layout */
						.side {
							float: left;
							width: 15%;
							margin-top:10px;
						}

						.middle {
							margin-top:10px;
							float: left;
							width: 70%;
						}

						/* Place text to the right */
						.right {
							text-align: right;
						}
					</style>
					<form action="/rate/{{ $dataFilm->id }}" method="POST" autocomplete="off" enctype="multipart/form-data">
						@csrf
						<div class="rate">
							<input type="radio" id="star5" name="rate" value="5" />
							<label for="star5" title="★★★★★">5 stars</label>
							<input type="radio" id="star4" name="rate" value="4" />
							<label for="star4" title="★★★★">4 stars</label>
							<input type="radio" id="star3" name="rate" value="3" />
							<label for="star3" title="★★★">3 stars</label>
							<input type="radio" id="star2" name="rate" value="2" />
							<label for="star2" title="★★">2 stars</label>
							<input type="radio" id="star1" name="rate" value="1" />
							<label for="star1" title="★">1 star</label>
						</div>
						<input type="hidden" name="id_film" value="{{ $dataFilm->id }}">
						<input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
						<div class="pl-lg-4">
							<div class="form-group">
								<div class="col-12 text-right">
									<a href="/rank" class="btn btn-xl btn-danger">Back</a>
									@if($dataFilm->trashed())
									<button class="btn btn-xl btn-primary" disabled>Submit</button>
									@elseif($dataRating === null)
									<button type="submit" class="btn btn-xl btn-primary">Submit</button>
									@elseif($dataRating->status === 'rated')
									<button class="btn btn-xl btn-primary" disabled>Anda Sudah Submit</button>
									@endif
								</div>
							</div>
						</div>
					</form>
					<h2>User Rating Untuk Film Ini</h2>
					<div class="side">
						<div>5 Star</div>
					</div>
					<div class="middle">
						<div class="bar-container">
							<div class="bar-5"></div>
						</div>
					</div>
					<div class="side right">
						<div>{{ $dataFilm->five_star }} Users</div>
					</div>
					<div class="side">
						<div>4 Star</div>
					</div>
					<div class="middle">
						<div class="bar-container">
							<div class="bar-4"></div>
						</div>
					</div>
					<div class="side right">
						<div>{{ $dataFilm->four_star }} Users</div>
					</div>
					<div class="side">
						<div>3 Star</div>
					</div>
					<div class="middle">
						<div class="bar-container">
							<div class="bar-3"></div>
						</div>
					</div>
					<div class="side right">
						<div>{{ $dataFilm->three_star }} Users</div>
					</div>
					<div class="side">
						<div>2 Star</div>
					</div>
					<div class="middle">
						<div class="bar-container">
							<div class="bar-2"></div>
						</div>
					</div>
					<div class="side right">
						<div>{{ $dataFilm->two_star }} Users</div>
					</div>
					<div class="side">
						<div>1 Star</div>
					</div>
					<div class="middle">
						<div class="bar-container">
							<div class="bar-1"></div>
						</div>
					</div>
					<div class="side right">
						<div>{{ $dataFilm->one_star }} Users</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
			<div class="card card-profile shadow">
				<div class="row justify-content-center">
					<div class="col-lg-3 order-lg-2">
						<div class="card-profile-image">
							<a href="#">
								<img src="/photo/film/{{ $dataFilm->photo }}" class="rounded-circle" id="film-img-tag">
							</a>
						</div>
					</div>
				</div>
				<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
					<div class="d-flex justify-content-between">
						@if($dataFilm->trashed())
						<a href="#" class="btn btn-sm btn-danger mr-4">Not Available</a>
						@else
						<a href="#" class="btn btn-sm btn-info mr-4">Available</a>
						@endif
						<a href="#" class="btn btn-sm btn-default float-right">{{ $dataFilm->total_stars }} Stars</a>
					</div>
				</div>
				<div class="card-body pt-0 pt-md-4">
					<div class="text-center mt-md-5">
						<h3 id="hasilnama">
							{{ $dataFilm->films_name }}
						</h3>
						<div class="h5 mt-4">
							<i class="ni business_briefcase-24 mr-2"></i>{{ $dataFilm->total_stars }} Stars - At RTV Official Website
						</div>
						<hr class="my-4" />
						<p id="hasildesc">{{ $dataFilm->desc }}</p>
						<a href="#">Show more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#film-img-tag').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#film-img").change(function(){
			readURL(this);
		});

		inputnama.oninput = function() {
			hasilnama.innerHTML = inputnama.value;
		};

		inputdesc.oninput = function() {
			hasildesc.innerHTML = inputdesc.value;
		};
	</script>

	@include('rtv.base.footer')