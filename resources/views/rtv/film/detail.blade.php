@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-8 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-12">
							<h3 class="mb-0">Detail Film {{ $dataFilm->films_name }}</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form action="/film/{{ $dataFilm->id }}" method="POST" autocomplete="off" enctype="multipart/form-data">
						@csrf
						<h6 class="heading-small text-muted mb-4">Film information</h6>
						<div class="pl-lg-4">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-username">Nama Film</label>
										<input type="text" name="films_name" class="form-control form-control-alternative" placeholder="Nama Film" value="{{ $dataFilm->films_name }}" id="inputnama">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-first-name">Photo</label>
										<input id="film-img" type="file" name="photo" class="form-control form-control-alternative" value="{{ $dataFilm->photo }}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="form-control-label" for="input-address">Description</label>
										<textarea id="inputdesc" rows="4" class="form-control form-control-alternative" placeholder="Deskripsi Film" name="desc" type="text" required>{{ $dataFilm->desc }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<hr class="my-4" />
						<div class="pl-lg-4">
							<div class="form-group">
								<div class="col-12 text-right">
									<a href="/film" class="btn btn-xl btn-danger">Back</a>
									@if($dataFilm->trashed())
									<button class="btn btn-xl btn-primary" disabled>Submit</button>
									@else
									<button type="submit" class="btn btn-xl btn-primary">Submit</button>
									@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
			<div class="card card-profile shadow">
				<div class="row justify-content-center">
					<div class="col-lg-3 order-lg-2">
						<div class="card-profile-image">
							<a href="#">
								<img src="/photo/film/{{ $dataFilm->photo }}" class="rounded-circle" id="film-img-tag">
							</a>
						</div>
					</div>
				</div>
				<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
					<div class="d-flex justify-content-between">
						@if($dataFilm->trashed())
						<a href="#" class="btn btn-sm btn-danger mr-4">Not Available</a>
						@else
						<a href="#" class="btn btn-sm btn-info mr-4">Available</a>
						@endif
						<a href="#" class="btn btn-sm btn-default float-right">{{ $dataFilm->total_stars }} Stars</a>
					</div>
				</div>
				<div class="card-body pt-0 pt-md-4">
					<div class="text-center mt-md-5">
						<h3 id="hasilnama">
							{{ $dataFilm->films_name }}
						</h3>
						<div class="h5 mt-4">
							<i class="ni business_briefcase-24 mr-2"></i>{{ $dataFilm->total_stars }} Stars - At RTV Official Website
						</div>
						<hr class="my-4" />
						<p id="hasildesc">{{ $dataFilm->desc }}</p>
						<a href="#">Show more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#film-img-tag').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#film-img").change(function(){
			readURL(this);
		});

		inputnama.oninput = function() {
			hasilnama.innerHTML = inputnama.value;
		};

		inputdesc.oninput = function() {
			hasildesc.innerHTML = inputdesc.value;
		};
	</script>

	@include('rtv.base.footer')