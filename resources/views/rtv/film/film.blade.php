@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<!-- Card stats -->
			<div class="row">
				
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-9 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-12">
							<h3 class="mb-0">Create Film / Acara TV</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form action="/film" method="POST" autocomplete="off" enctype="multipart/form-data">
						@csrf
						<h6 class="heading-small text-muted mb-4">Film / Acara TV information</h6>
						<div class="pl-lg-4">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-username">Nama Film / Acara TV</label>
										<input type="text" name="films_name" class="form-control form-control-alternative" id="inputnama" placeholder="Nama Film" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-first-name">Photo</label>
										<input type="file" name="photo" class="form-control form-control-alternative" accept=".gif,.jpg,.jpeg,.png" id="film-img" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="form-control-label" for="input-address">Description</label>
										<textarea id="inputdesc" rows="4" class="form-control form-control-alternative" placeholder="Deskripsi Film" name="desc" type="text" required></textarea>
									</div>
								</div>
							</div>
						</div>
						<hr class="my-4" />
						<div class="pl-lg-4">
							<div class="form-group">
								<div class="col-12 text-right">
									<a href="/" class="btn btn-xl btn-danger">Back</a>
									<button type="submit" class="btn btn-xl btn-primary">Submit</button>
								</div>
							</div>
						</div>
						<input type="hidden" name="one_star" value="0">
						<input type="hidden" name="two_star" value="0">
						<input type="hidden" name="three_star" value="0">
						<input type="hidden" name="four_star" value="0">
						<input type="hidden" name="five_star" value="0">
						<input type="hidden" name="total_stars" value="0">
					</form>
				</div>
			</div>
		</div>
		<style type="text/css">
			.cardbox img {
				border-radius: 5px 5px 0 0;
				width: 100%;
				margin-bottom: -50px;
			}
		</style>
		<div class="col-xl-3 order-xl-2 mb-5 mb-xl-0">
			<div class="card card-profile shadow">
				<div class="row">
					<div class="cardbox col-md-12">
						<img src="/photo/product/endgame.png" id="film-img-tag">
					</div>
				</div>
				<div class="card-body pt-0 pt-md-4">
					<div class="text-center mt-md-5">
						<h3 id="hasilnama">
							Avengers: Endgame
						</h3>
						<div class="h5 font-weight-300">
							<i class="ni location_pin mr-2"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i>
						</div>
						<hr class="my-4" />
						<p id="hasildesc">Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script>
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#film-img-tag').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#film-img").change(function(){
			readURL(this);
		});

		inputnama.oninput = function() {
			hasilnama.innerHTML = inputnama.value;
		};

		inputdesc.oninput = function() {
			hasildesc.innerHTML = inputdesc.value;
		};
	</script>
	<div class="row mt-5">
		<div class="col-xl-12 mb-5 mb-xl-0">
			<div class="card shadow">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0">Daftar Film</h3>
						</div>
						<div class="col text-right">
							<a href="#!" class="btn btn-sm btn-primary">See all</a>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<!-- Projects table -->
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col">No</th>
								<th scope="col">Foto</th>
								<th scope="col">Nama Film</th>
								<th scope="col">Total Stars</th>
								<th scope="col">Status</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@foreach($dataFilm as $flm)
							<tr>
								<td>
									{{ $number++ }}
								</td>
								<td>
									<span class="avatar avatar-sm rounded-circle">
										<img src="/photo/film/{{ $flm->photo }}">
									</span>
								</td>
								<th scope="row">
									{{ $flm->films_name }}
								</th>
								<td>
									{{ $flm->total_stars }}
								</td>
								<td>
									@if($flm->trashed())
									<span class="badge badge-dot mr-4">
										<i class="bg-danger"></i> Deleted
									</span>
									@else
									<span class="badge badge-dot mr-4">
										<i class="bg-success"></i> Available
									</span>
									@endif
								</td>
								<td>
									<a href="/film/detail/{{ $flm->id }}" class="btn btn-sm btn-primary">Detail</a>
									@if($flm->trashed())
									<a href="/film/destroy/{{ $flm->id }}" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin memnghapus permanen film / tayangan {{ $flm->name }}?')">Permanent Delete</a>
									@else
									<a href="/film/delete/{{ $flm->id }}" class="btn btn-sm btn-warning" onclick="return confirm('Apakah Anda yakin memnghapus film / tayangan {{ $flm->name }}?')">Delete</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div style="margin-left: 40%">
						{!! $dataFilm->appends(request()->all())->links() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('rtv.base.footer')