@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<!-- Card stats -->
			<div class="row">
				<div class="col-xl-12 col-lg-12">
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<h5 class="card-title text-uppercase text-muted mb-0">Admin Dashboard</h5>
									<span class="h2 font-weight-bold mb-0">Data User</span>
								</div>
								<div class="col-auto">
									<div class="icon icon-shape bg-info text-white rounded-circle shadow">
										<i class="fas fa-user"></i>
									</div>
								</div>
							</div>
							<p class="mt-3 mb-0 text-muted text-sm">
								<span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
								<span class="text-nowrap">Since last month</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row mt-5">
		<div class="col-xl-12 mb-5 mb-xl-0">
			<div class="card shadow">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0">Daftar User</h3>
						</div>
						<div class="col text-right">
							<a href="#!" class="btn btn-sm btn-primary">See all</a>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<!-- Projects table -->
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col">No</th>
								<th scope="col">Foto</th>
								<th scope="col">Nama</th>
								<th scope="col">Email</th>
								<th scope="col">Role</th>
								<th scope="col">Status</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@foreach($dataUser as $usr)
							<tr>
								<td>
									{{ $number++ }}
								</td>
								<td>
									<span class="avatar avatar-sm rounded-circle">
										<img src="/photo/product/{{ $usr->photo }}">
									</span>
								</td>
								<th scope="row">
									{{ $usr->name }}
								</th>
								<td>
									{{ $usr->email }}
								</td>
								<td>
									{{ $usr->role }}
								</td>
								<td>
									@if($usr->trashed())
									<span class="badge badge-dot mr-4">
										<i class="bg-danger"></i> Banned
									</span>
									@else
									<span class="badge badge-dot mr-4">
										<i class="bg-success"></i> Available
									</span>
									@endif
								</td>
								<td>
									<a href="/user/detail/{{ $usr->id }}" class="btn btn-sm btn-primary">Detail</a>
									@if($usr->trashed())
									<a href="/user/destroy/{{ $usr->id }}" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin memnghapus user {{ $usr->name }}?')">Delete</a>
									@else
									<a href="/user/delete/{{ $usr->id }}" class="btn btn-sm btn-warning" onclick="return confirm('Apakah Anda yakin membanned user {{ $usr->name }}?')">Banned</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<div style="margin-left: 40%">
						{!! $dataUser->appends(request()->all())->links() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('rtv.base.footer')