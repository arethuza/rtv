@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 600px; background-image: url({{asset('assets/')}}/img/theme/endgame-wallpaper.jpg); background-size: cover; background-position: center top;">
	<!-- Mask -->
	<span class="mask bg-gradient-default opacity-8"></span>
	<!-- Header container -->
	<div class="container-fluid d-flex align-items-center">
		<div class="row">
			<div class="col-lg-7 col-md-10">
				<h1 class="display-2 text-white">Rate Your Fav Movies</h1>
				<p class="text-white mt-0 mb-5">Pilih dan rating film favoritmu, kamu bisa cek daftar film yang paling populer di opsi Rank, tentu saja dengan rekomendasi film lainnya.</p>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-12 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-12">
							<form action="/rate" method="GET" autocomplete="off">
								<h3 class="mb-0">Film List</h3>
								<span class="float-right">
									<input type="text" name="search" class="form-control col-md-12 col-xs-12" placeholder="🍭 Search here ...">
								</span>
							</form>
						</div>
					</div>
				</div>
				<style type="text/css">
					.cardbox {
						box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
						transition: 0.3s;
						width: 100%;
						height: 440px;
						border-radius: 5px;
						margin-bottom: 40px;
					}

					.cardbox:hover {
						box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
					}

					.cardbox img {
						border-radius: 5px 5px 0 0;
						width: 100%;
						height: 285px;
						margin-bottom: 10px;
					}

					.cardbox p {
						padding-bottom: 10px;
					}

					.cardbox .container {
						text-align: center;
					}
				</style>
				<div class="card-body">
					<div class="row">
						@foreach($dataFilm as $flm)
						<div class="col-lg-3 col-md-6">
							<a href="/rate/detail/{{ $flm->id }}">
								<div class="cardbox">
									<img src="/photo/film/{{ $flm->photo }}" alt="Avatar" style="width:100%">
									<div class="container">
										<h3><b>{{ $flm->films_name }}</b></h3>
										<h5><b>{{ $flm->total_stars }} Stars</b></h5>
										<p>{{ str_limit($flm->desc, 55) }}</p> 
									</div>
								</div>
							</a>
						</div>
						@endforeach
					</div>
				</div>
				<div style="margin-left: 40%">
					{!! $dataFilm->appends(request()->all())->links() !!}
				</div>
			</div>
		</div>
	</div>
	
	@include('rtv.base.footer')