	  <!-- Footer -->
	  <footer class="footer">
	  	<div class="row align-items-center justify-content-xl-between">
	  		<div class="col-xl-6">
	  			<div class="copyright text-center text-xl-left text-muted">
	  				&copy; 2018 <a href="https://steamcommunity.com/id/therethuza" class="font-weight-bold ml-1" target="_blank">Arethuza</a>
	  			</div>
	  		</div>
	  		<div class="col-xl-6">
	  			<ul class="nav nav-footer justify-content-center justify-content-xl-end">
	  				<li class="nav-item">
	  					<a href="http://netorared.000webhostapp.com/" class="nav-link" target="_blank">Arethuza</a>
	  				</li>
	  				<li class="nav-item">
	  					<a href="http://netorared.000webhostapp.com/" class="nav-link" target="_blank">About Us</a>
	  				</li>
	  				<li class="nav-item">
	  					<a href="http://netorared.000webhostapp.com/" class="nav-link" target="_blank">Blog</a>
	  				</li>
	  			</ul>
	  		</div>
	  	</div>
	  </footer>
	</div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="{{asset('assets/')}}/vendor/jquery/dist/jquery.min.js"></script>
<script src="{{asset('assets/')}}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Optional JS -->
<script src="{{asset('assets/')}}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{asset('assets/')}}/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="{{asset('assets/')}}/js/argon.js?v=1.0.0"></script>
</body>

</html>