<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
	<meta name="author" content="Arethuza">
	<title>RTV Official - Rate your favorite Movie or TV Show</title>
	<!-- Favicon -->
	<link href="{{asset('assets/')}}/img/brand/favicon.png" rel="icon" type="image/png">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<!-- Icons -->
	<link href="{{asset('assets/')}}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
	<link href="{{asset('assets/')}}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
	<!-- Argon CSS -->
	<link type="text/css" href="{{asset('assets/')}}/css/argon.css?v=1.0.0" rel="stylesheet">
</head>

<body>
	<!-- Sidenav -->
	<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
		<div class="container-fluid">
			<!-- Toggler -->
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<!-- Brand -->
			<a class="navbar-brand pt-0" href="./index.html">
				<img src="{{asset('assets/')}}/img/brand/blue2.png" class="navbar-brand-img" alt="...">
			</a>
			<!-- User -->
			<ul class="nav align-items-center d-md-none">
				<li class="nav-item dropdown">
					<a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="ni ni-bell-55"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<div class="media align-items-center">
							<span class="avatar avatar-sm rounded-circle">
								<img alt="Image placeholder" src="{{asset('assets/')}}/img/theme/team-1-800x800.jpg">
							</span>
						</div>
					</a>
					<div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
						<div class=" dropdown-header noti-title">
							<h6 class="text-overflow m-0">Welcome!</h6>
						</div>
						<a href="/profile/{{ Auth::user()->id }}" class="dropdown-item">
							<i class="ni ni-single-02"></i>
							<span>My profile</span>
						</a>
						<a href="./examples/profile.html" class="dropdown-item">
							<i class="ni ni-settings-gear-65"></i>
							<span>Settings</span>
						</a>
						<a href="./examples/profile.html" class="dropdown-item">
							<i class="ni ni-calendar-grid-58"></i>
							<span>Activity</span>
						</a>
						<a href="./examples/profile.html" class="dropdown-item">
							<i class="ni ni-support-16"></i>
							<span>Support</span>
						</a>
						<div class="dropdown-divider"></div>
						<a href="{{ route('logout') }}" class="dropdown-item" 
						onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">
						<i class="ni ni-user-run"></i>
						<span>Logout</span>
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</li>
		</ul>
		<!-- Collapse -->
		<div class="collapse navbar-collapse" id="sidenav-collapse-main">
			<!-- Collapse header -->
			<div class="navbar-collapse-header d-md-none">
				<div class="row">
					<div class="col-6 collapse-brand">
						<a href="./index.html">
							<img src="{{asset('assets/')}}/img/brand/blue2.png">
						</a>
					</div>
					<div class="col-6 collapse-close">
						<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
							<span></span>
							<span></span>
						</button>
					</div>
				</div>
			</div>
			<!-- Navigation -->
			<ul class="navbar-nav">
				@if(Auth::user()->role == "admin")
				<li class="nav-item">
					<a class="nav-link" href="/">
						<i class="ni ni-tv-2 text-primary"></i> Dashboard
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/user">
						<i class="ni ni-circle-08 text-yellow"></i> Data User
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/film">
						<i class="ni ni-camera-compact text-orange"></i> Data Film
					</a>
				</li>
				@else
				<li class="nav-item">
					<a class="nav-link" href="/rate">
						<i class="ni ni-tv-2 text-primary"></i> Dashboard
					</a>
				</li>
				@endif
				<li class="nav-item">
					<a class="nav-link" href="/rate">
						<i class="ni ni-bullet-list-67 text-red"></i> Rate
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/profile/{{ Auth::user()->id }}">
						<i class="ni ni-single-02 text-pink"></i> Profile
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/rank">
						<i class="ni ni-planet text-blue"></i> Rank
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- Main content -->
<div class="main-content">
	<!-- Top navbar -->
	<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
		<div class="container-fluid">