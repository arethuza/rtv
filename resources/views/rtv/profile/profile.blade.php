@include('rtv.base.header')
<!-- Brand -->
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Dashboard</a>
@include('rtv.base.navbar')
<!-- Header -->
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 600px; background-image: url({{asset('assets/')}}/img/theme/wewe2.png); background-size: cover; background-position: center top;">
	<!-- Mask -->
	<span class="mask bg-gradient-default opacity-8"></span>
	<!-- Header container -->
	<div class="container-fluid d-flex align-items-center">
		<div class="row">
			<div class="col-lg-7 col-md-10">
				<h1 class="display-2 text-white">Hai {{ $dataUser->name }}, Ini Profilmu</h1>
				<p class="text-white mt-0 mb-5">Kamu bisa edit profilmu sepuasnya, tidak ada limit ataupun batasan!, input ini sudah sinkron dengan kartu profilmu, jadi kamu bisa mencocokkannya sebelum submit :)</p>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-8 order-xl-1">
			<div class="card bg-secondary shadow">
				<div class="card-header bg-white border-0">
					<div class="row align-items-center">
						<div class="col-12">
							<h3 class="mb-0">Edit Profile</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					@if ($errors->any())
					<div class="col-sm-12">
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</div>
					</div>
					@endif
					<form action="/profile/{{ Auth::user()->id }}" method="POST" autocomplete="off" enctype="multipart/form-data">
						@csrf
						<h6 class="heading-small text-muted mb-4">User information</h6>
						<div class="pl-lg-4">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-username">Nama User</label>
										<input type="text" id="inputnama" name="name" class="form-control form-control-alternative" placeholder="Nama User" value="{{ $dataUser->name }}">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-email">Email address</label>
										<input type="email" name="email" class="form-control form-control-alternative" placeholder="user@example.com" value="{{ $dataUser->email }}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-first-name">Photo</label>
										<input id="profile-img" type="file" name="photo" class="form-control form-control-alternative" accept=".gif,.jpg,.jpeg,.png" value="{{ $dataUser->photo }}">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-country">Phone</label>
										<input type="number" id="inputhp" name="phone" class="form-control form-control-alternative" placeholder="Phone Number" value="{{ $dataUser->phone }}">
									</div>
								</div>
								@if(Auth::user()->role == "admin")
								<input type="hidden" name="role" value="admin">
								@else
								<input type="hidden" name="role" value="user">
								@endif
							</div>
						</div>
						<hr class="my-4" />
						<!-- Address -->
						<h6 class="heading-small text-muted mb-4">Contact information</h6>
						<div class="pl-lg-4">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="form-control-label" for="input-address">Address</label>
										<textarea id="inputalamat" rows="4" class="form-control form-control-alternative" placeholder="Home Address" name="address" type="text">{{ $dataUser->address }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<hr class="my-4" />
						<div class="pl-lg-4">
							<div class="form-group">
								<div class="col-12 text-right">
									<a href="/user" class="btn btn-xl btn-danger">Back</a>
									@if($dataUser->trashed())
									<button class="btn btn-xl btn-primary" disabled>Submit</button>
									@else
									<button type="submit" class="btn btn-xl btn-primary">Edit</button>
									@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
			<div class="card card-profile shadow">
				<div class="row justify-content-center">
					<div class="col-lg-3 order-lg-2">
						<div class="card-profile-image">
							<a href="#">
								<img src="/photo/product/{{ $dataUser->photo }}" class="rounded-circle" id="profile-img-tag">
							</a>
						</div>
					</div>
				</div>
				<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
					<div class="d-flex justify-content-between">
						@if($dataUser->trashed())
						<a href="#" class="btn btn-sm btn-danger mr-4">Banned</a>
						@else
						<a href="#" class="btn btn-sm btn-info mr-4">Available</a>
						@endif
						<a href="#" class="btn btn-sm btn-default float-right">id: {{ Auth::user()->id }}</a>
					</div>
				</div>
				<div class="card-body pt-0 pt-md-4">
					<div class="text-center mt-md-5">
						<h3 id="hasilnama">
							{{ $dataUser->name }}
						</h3>
						<div class="h5 font-weight-300">
							<i class="ni location_pin mr-2"></i><span id="hasilalamat">{{ $dataUser->address }}</span>, <span id="hasilhp">{{ $dataUser->phone }}</span>
						</div>
						<div class="h5 mt-4">
							<i class="ni business_briefcase-24 mr-2"></i>{{ ucfirst(trans($dataUser->role)) }} - At RTV Official Website
						</div>
						<div>
							<i class="ni education_hat mr-2"></i>{{ $dataUser->email }}
						</div>
						<hr class="my-4" />
						<p>Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.</p>
						<a href="#">Show more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script type="text/javascript">
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#profile-img-tag').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#profile-img").change(function(){
			readURL(this);
		});

		inputnama.oninput = function() {
			hasilnama.innerHTML = inputnama.value;
		};

		inputalamat.oninput = function() {
			hasilalamat.innerHTML = inputalamat.value;
		};

		inputhp.oninput = function() {
			hasilhp.innerHTML = inputhp.value;
		};
	</script>

	@include('rtv.base.footer')