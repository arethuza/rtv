<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// verif email user
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/", "DashboardController@index");

Route::prefix('user')->group(function(){
	Route::get("/", "UserController@index");
	Route::post("/", "UserController@store");
	Route::get('/detail/{id}', 'UserController@detail');
	Route::post('/{id}', 'UserController@update');
	Route::get('/delete/{id}', 'UserController@banned');
	Route::get('/destroy/{id}', 'UserController@delete');
});

Route::prefix('film')->group(function(){
	Route::get("/", "FilmController@index");
	Route::post("/", "FilmController@store");
	Route::get('/detail/{id}', 'FilmController@detail');
	Route::post('/{id}', 'FilmController@update');
	Route::get('/delete/{id}', 'FilmController@banned');
	Route::get('/destroy/{id}', 'FilmController@delete');
});

Route::prefix('rate')->group(function(){
	Route::get("/", "RateController@index");
	Route::post('/{id}', 'RateController@store');
	Route::get('/detail/{id}', 'RateController@detail');
});

Route::prefix('rank')->group(function(){
	Route::get("/", "RankController@index");
	Route::post('/{id}', 'RankController@store');
	Route::get('/detail/{id}', 'RankController@detail');
});

Route::prefix('profile')->group(function(){
	Route::get("/{id}", "ProfileController@index");
	Route::post("/{id}", "ProfileController@update");
});

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Auth::routes();