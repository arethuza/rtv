<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'API\Auth\LoginsController@store');
Route::get('/logout', 'API\Auth\LoginsController@index');

Route::group(['middleware' => 'auth:api'], function(){
	Route::apiResource('user', 'API\UsersController');
	Route::apiResource('film', 'API\FilmsController');
	Route::apiResource('rank', 'API\RanksController');
});