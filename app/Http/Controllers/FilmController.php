<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Film;
use Session;
use Validator;
use Exception;
use SoftDeletes;

class FilmController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

	public function index()
	{
		if (Auth::user()->role != 'admin') {
    		return redirect('/rate');
    	}

		$dataFilm = Film::query();

		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$dataFilm->where(
				"films_name", "like", "%" . request()->query("search") . "%"
			);
		}

		$pagination = 5;
		$dataFilm = $dataFilm->withTrashed()->latest()->paginate($pagination);

		$number = 1;

		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}

		return view('rtv.film.film', compact('dataFilm', 'number'));
	}

	public function store(Request $request)
	{
		try{
			$this->validate($request,[
				'photo'	=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
			]);

			\DB::beginTransaction();

			$imgName = time().'.'.request()->photo->getClientOriginalExtension();
			request()->photo->move(public_path('photo\film'), $imgName);

            //QUERY STORE
			$dataFilm = new Film();
			$dataFilm->films_name = $request->films_name;
			$dataFilm->desc = $request->desc;
			$dataFilm->one_star = $request->one_star;
			$dataFilm->two_star = $request->two_star;
			$dataFilm->three_star = $request->three_star;
			$dataFilm->four_star = $request->four_star;
			$dataFilm->five_star = $request->five_star;
			$dataFilm->total_stars = $request->total_stars;
			$dataFilm->photo = $imgName;
			$dataFilm->save();
			\DB::commit();

			Session::flash('success', 'Berhasil menambahkan data!');
			return redirect()->back();
		}catch (Exception $e){
			\DB::rollBack();
			Session::flash('success', 'Gagal menambahkan data!');
			return redirect()->back()->with($e);
		}
	}

	public function detail($id)
	{
		if (Auth::user()->role != 'admin') {
    		return redirect('/rate');
    	}
    	
		$dataFilm = Film::where('id', $id)->withTrashed()->first();

		return view('rtv.film.detail', compact('dataFilm'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'films_name' => 'required',     
			'desc' => 'required',
			'photo'	=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);

		$dataFilm = Film::find($id);

		$imgName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('photo\film'), $imgName);

		$dataFilm->films_name = $request->films_name;
		$dataFilm->desc = $request->desc;
		$dataFilm->photo = $imgName;
		$dataFilm->save();

		return redirect()->back();
	}

	public function banned($id)
	{
		$dataFilm = Film::find($id);
		$dataFilm->delete();

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataFilm = Film::where('id', $id)->withTrashed()->first();
		$dataFilm->forceDelete();

		return redirect()->back();
	}
}
