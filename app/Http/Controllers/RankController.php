<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Film;
use App\Rating;
use App\User;
use Session;
use Validator;
use Exception;

class RankController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index()
	{
		$dataFilm = Film::query();
		$dataUser = User::all();
		$ratingUser = Film::sum('total_stars');
		$sudahRating = Film::where('total_stars', '>', '0')->get();
		
		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$dataFilm->where(
				"films_name", "like", "%" . request()->query("search") . "%"
			);
		}

		$pagination = 10;
		$dataFilm = $dataFilm->withTrashed()->orderBy('total_stars', 'desc')->paginate($pagination);

		$number = 1;

		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}

		return view('rtv.rank.rank', compact('dataFilm', 'number', 'dataUser', 'ratingUser', 'sudahRating'));
	}

	public function detail($id)
	{
		$dataFilm = Film::where('id', $id)->withTrashed()->first();
		$ids = Auth::user()->id;
		$dataBagi = $dataFilm->five_star + $dataFilm->four_star + $dataFilm->three_star + $dataFilm->two_star + $dataFilm->one_star;
		$dataRating = Rating::where('user_id', $ids)->where('film_id', $id)->first();
		$p5 = ($dataFilm->five_star * 100) / $dataBagi;
		$p4 = ($dataFilm->four_star * 100) / $dataBagi;
		$p3 = ($dataFilm->three_star * 100) / $dataBagi;
		$p2 = ($dataFilm->two_star * 100) / $dataBagi;
		$p1 = ($dataFilm->one_star * 100) / $dataBagi;

		return view('rtv.rank.detail', compact('dataFilm', 'dataRating', 'p5', 'p4', 'p3', 'p2', 'p1'));
	}
}
