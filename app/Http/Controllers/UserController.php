<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;
use Validator;
use Exception;
use SoftDeletes;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
	public function index()
	{
		if (Auth::user()->role != 'admin') {
    		return redirect('/rate');
    	}

		$dataUser = User::query();

		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$dataUser->where(
				"name", "like", "%" . request()->query("search") . "%"
			);
		}

		$pagination = 5;
		$dataUser = $dataUser->withTrashed()->latest()->paginate($pagination);

		$number = 1;

		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}

		return view('rtv.user.user', compact('dataUser', 'number'));
	}

	public function store(Request $request)
	{
		try{
			$this->validate($request,[
				'name' => 'required',     
				'email' => 'required',
				'password' => 'required',
				'role' => 'required',
				'address' => 'required',
				'phone' => 'required',
				'photo'	=> 'required'
			]);

			\DB::beginTransaction();

			$imgName = time().'.'.request()->photo->getClientOriginalExtension();
			request()->photo->move(public_path('photo\product'), $imgName);

            //QUERY STORE
			$dataUser = new User();
			$dataUser->name = $request->name;
			$dataUser->email = $request->email;
			$dataUser->password = bcrypt($request->password);
			$dataUser->role = $request->role;
			$dataUser->address = $request->address;
			$dataUser->phone = $request->phone;
			$dataUser->photo = $imgName;
			$dataUser->save();
			\DB::commit();

			Session::flash('success', 'Berhasil menambahkan data!');
			return redirect()->back();
		}catch (Exception $e){
			\DB::rollBack();
			Session::flash('success', 'Gagal menambahkan data!');
			return redirect()->back()->with($e);
		}
	}

	public function detail($id)
	{
		if (Auth::user()->role != 'admin') {
    		return redirect('/rate');
    	}
    	
		$dataUser = User::where('id', $id)->withTrashed()->first();

		return view('rtv.user.detail', compact('dataUser'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'name' => 'required',     
			'email' => 'required',
			'role' => 'required',
			'address' => 'required',
			'phone' => 'required',
			'photo'	=> 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:20480'
		]);

		$dataUser = User::find($id);

		$imgName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('photo\product'), $imgName);

		$dataUser->name = $request->name;
		$dataUser->email = $request->email;
		$dataUser->role = $request->role;
		$dataUser->address = $request->address;
		$dataUser->phone = $request->phone;
		$dataUser->photo = $imgName;
		$dataUser->save();

		$dataUser->save();

		return redirect()->back();
	}

	public function banned($id)
	{
		$dataUser = User::find($id);
		$dataUser->delete();

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataUser = User::where('id', $id)->withTrashed()->first();
		$dataUser->forceDelete();

		return redirect()->back();
	}
}