<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Resources\UsersCollection;
use App\User;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counter = 1;
        $dataFilm = new UsersCollection(User::query()->paginate(10));
        return apiResponseSuccess('OK!', $dataFilm, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            \DB::beginTransaction();

            //QUERY STORE
            $dataStudio = new User();
            $dataStudio->name = $request->name;
            $dataStudio->email = $request->email;
            $dataStudio->password = $request->password;
            $dataStudio->role = 'user';
            $dataStudio->save();
            \DB::commit();

            return apiResponseSuccess('Berhasil Tambah!', $dataStudio, 200);
        }catch (Exception $e){
            \DB::rollBack();
            return apiResponseErrors('Gagal Tambah!', [
                'Jeng jenge error'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataFilm = new UsersCollection(User::where('id', $id)->paginate(5));
        return apiResponseSuccess('OQ!', $dataFilm, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
