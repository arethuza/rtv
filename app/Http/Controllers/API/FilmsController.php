<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Resources\FilmsCollection;
use App\Film;
use App\Http\Controllers\Controller;

class FilmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counter = 1;
        $dataFilm = new FilmsCollection(Film::query()->paginate(10));
        return apiResponseSuccess('OK!', $dataFilm, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            \DB::beginTransaction();

            //QUERY STORE
            $dataStudio = new Film();
            $dataStudio->films_name = $request->name;
            $dataStudio->one_star = '0';
            $dataStudio->two_star = '0';
            $dataStudio->three_star = '0';
            $dataStudio->four_star = '0';
            $dataStudio->five_star = '0';
            $dataStudio->total_stars = '0';
            $dataStudio->desc = $request->desc;
            $dataStudio->photo = '1557817886.jpg';
            $dataStudio->save();
            \DB::commit();

            return apiResponseSuccess('Berhasil Tambah!', $dataStudio, 200);
        }catch (Exception $e){
            \DB::rollBack();
            return apiResponseErrors('Gagal Tambah!', [
                'Jeng jenge error'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataFilm = new FilmsCollection(Film::where('id', $id)->paginate(5));
        return apiResponseSuccess('OQ!', $dataFilm, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
