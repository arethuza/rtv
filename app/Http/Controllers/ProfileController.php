<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;
use Validator;
use Exception;
use SoftDeletes;


class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index($id)
	{
		if ($id != Auth::user()->id) {
    		return redirect('/rate');
    	}
		
		$dataUser = User::where('id', $id)->withTrashed()->first();

		return view('rtv.profile.profile', compact('dataUser'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'name' => 'required',     
			'email' => 'required',
			'role' => 'required',
			'address' => 'required',
			'phone' => 'required',
			'photo'	=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
		]);

		$dataUser = User::find($id);

		$imgName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('photo\product'), $imgName);

		$dataUser->name = $request->name;
		$dataUser->email = $request->email;
		$dataUser->role = $request->role;
		$dataUser->address = $request->address;
		$dataUser->phone = $request->phone;
		$dataUser->photo = $imgName;
		$dataUser->save();

		$dataUser->save();

		return redirect()->back();
	}
}
