<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index()
    {
    	if (Auth::user()->role != 'admin') {
    		return redirect('/rate');
    	}
    	return view('rtv.dashboard.dashboard');
    }
}
