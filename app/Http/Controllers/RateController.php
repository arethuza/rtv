<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Film;
use App\Rating;
use Session;
use Validator;
use Exception;

class RateController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
    public function index()
	{
		$dataFilm = Film::query();
		$ids = Auth::user()->id;
		$dataRating = Rating::where('user_id', $ids)->first();

		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$dataFilm->where(
				"films_name", "like", "%" . request()->query("search") . "%"
			);
		}

		$pagination = 8;
		$dataFilm = $dataFilm->withTrashed()->latest()->paginate($pagination);

		$number = 1;

		if (request()->has('page') && request()->get('page') > 1) {
			$number += (request()->get('page') - 1) * $pagination;
		}

		return view('rtv.rate.rate', compact('dataFilm', 'number', 'dataRating'));
	}

	public function detail($id)
	{
		$dataFilm = Film::where('id', $id)->withTrashed()->first();
		$ids = Auth::user()->id;
		$dataBagi = $dataFilm->five_star + $dataFilm->four_star + $dataFilm->three_star + $dataFilm->two_star + $dataFilm->one_star;
		$dataRating = Rating::where('user_id', $ids)->where('film_id', $id)->first();
		if ($dataBagi == 0) {
			$p5 = 0;
			$p4 = 0;
			$p3 = 0;
			$p2 = 0;
			$p1 = 0;
		} else {
			$p5 = ($dataFilm->five_star * 100) / $dataBagi;
			$p4 = ($dataFilm->four_star * 100) / $dataBagi;
			$p3 = ($dataFilm->three_star * 100) / $dataBagi;
			$p2 = ($dataFilm->two_star * 100) / $dataBagi;
			$p1 = ($dataFilm->one_star * 100) / $dataBagi;
		}

		return view('rtv.rate.detail', compact('dataFilm', 'dataRating', 'p5', 'p4', 'p3', 'p2', 'p1'));
	}

	public function store(Request $request)
	{
		try{
			$this->validate($request,[
				'rate' => 'required',
			]);

			DB::beginTransaction();

            //QUERY STORE
			$dataRate = new Rating();
			$dataRate->rating = $request->rate;
			$dataRate->film_id = $request->id_film;
			$dataRate->user_id = $request->id_user;
			$dataRate->status = 'rated';

			$dataRate->save();

			if ($dataRate->rating == 1) {
				Film::find($request->id_film)->increment('total_stars', 1);
				Film::find($request->id_film)->increment('one_star', 1);
			} elseif ($dataRate->rating == 2) {
				Film::find($request->id_film)->increment('total_stars', 2);
				Film::find($request->id_film)->increment('two_star', 1);
			} elseif ($dataRate->rating == 3) {
				Film::find($request->id_film)->increment('total_stars', 3);
				Film::find($request->id_film)->increment('three_star', 1);
			} elseif ($dataRate->rating == 4) {
				Film::find($request->id_film)->increment('total_stars', 4);
				Film::find($request->id_film)->increment('four_star', 1);
			} else {
				Film::find($request->id_film)->increment('total_stars', 5);
				Film::find($request->id_film)->increment('five_star', 1);
			}
			DB::commit();

			Session::flash('success', 'Berhasil menambahkan data!');
			return redirect()->back();
		}catch (Exception $e){
			DB::rollBack();
			Session::flash('success', 'Gagal menambahkan data!');
			return redirect()->back()->with($e);
		}
	}
}
