<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilmsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama film' => $this->films_name,
            'total rating' => $this->total_stars,
            'deskripsi' => $this->desc,
            'photo' => $this->photo,
        ];
    }
}
