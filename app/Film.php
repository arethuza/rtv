<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Film extends Model
{
    use SoftDeletes;

    protected $table = "films";

    protected $fillable = ["id", "films_name", "desc", "photo"];
}
