<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = "film_rating";

    protected $fillable = ["id","user_id", "film_id", "status", "rating"];
}
